package com.example.parqueaderoFacil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParqueaderoFacilApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParqueaderoFacilApplication.class, args);
	}

}
