-- MySQL Script generated by MySQL Workbench
-- Tue Aug 30 11:05:28 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema parqueaderoFacil
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `parqueaderoFacil` ;

-- -----------------------------------------------------
-- Schema parqueaderoFacil
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `parqueaderoFacil` DEFAULT CHARACTER SET utf8 ;
USE `parqueaderoFacil` ;

-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Adminstrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Adminstrador` (
  ` Cedula` INT NOT NULL,
  `contraseña` VARCHAR(45) NOT NULL,
  PRIMARY KEY (` Cedula`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `ID_Usuario_UNIQUE` ON `parqueaderoFacil`.`Adminstrador` (` Cedula` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Registro_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Registro_user` (
  `Cedula` INT NOT NULL,
  `Nombre_apellido` VARCHAR(45) NOT NULL,
  `Identificacion` LONGBLOB NOT NULL,
  `Contraseña` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Direccion` VARCHAR(45) NOT NULL,
  `Telefono` LONGBLOB NOT NULL,
  `Adminstrador_ Cedula` INT NOT NULL,
  PRIMARY KEY (`Cedula`),
  CONSTRAINT `fk_Registro_user_Adminstrador1`
    FOREIGN KEY (`Adminstrador_ Cedula`)
    REFERENCES `parqueaderoFacil`.`Adminstrador` (` Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `email_UNIQUE` ON `parqueaderoFacil`.`Registro_user` (`Email` ASC) VISIBLE;

CREATE UNIQUE INDEX `Identificacion_UNIQUE` ON `parqueaderoFacil`.`Registro_user` (`Identificacion` ASC) VISIBLE;

CREATE INDEX `fk_Registro_user_Adminstrador1_idx` ON `parqueaderoFacil`.`Registro_user` (`Adminstrador_ Cedula` ASC) VISIBLE;

CREATE UNIQUE INDEX `Adminstrador_ Cedula_UNIQUE` ON `parqueaderoFacil`.`Registro_user` (`Adminstrador_ Cedula` ASC) VISIBLE;

CREATE UNIQUE INDEX `Telefono_UNIQUE` ON `parqueaderoFacil`.`Registro_user` (`Telefono` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Vehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Vehiculo` (
  `Placa` VARCHAR(6) NOT NULL,
  `Marca` VARCHAR(15) NOT NULL,
  `Modelo` INT(4) NOT NULL,
  `Tipo_Vehiculo` TINYINT(1) NOT NULL,
  `Registro_user_Cedula` INT NOT NULL,
  PRIMARY KEY (`Registro_user_Cedula`),
  CONSTRAINT `fk_Vehiculo_Registro_user1`
    FOREIGN KEY (`Registro_user_Cedula`)
    REFERENCES `parqueaderoFacil`.`Registro_user` (`Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Vehiculo_Registro_user1_idx` ON `parqueaderoFacil`.`Vehiculo` (`Registro_user_Cedula` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Factura` (
  `Cod_Factura` INT NOT NULL,
  `tarifa` FLOAT NOT NULL,
  `Adminstrador_ Cedula` INT NULL,
  PRIMARY KEY (`Cod_Factura`),
  CONSTRAINT `fk_Factura_Adminstrador1`
    FOREIGN KEY (`Adminstrador_ Cedula`)
    REFERENCES `parqueaderoFacil`.`Adminstrador` (` Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cedula_user`
    FOREIGN KEY (`Cod_Factura`)
    REFERENCES `parqueaderoFacil`.`Vehiculo` (`Registro_user_Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Factura_Adminstrador1_idx` ON `parqueaderoFacil`.`Factura` (`Adminstrador_ Cedula` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`User_Particular`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`User_Particular` (
  `contraseña` VARCHAR(45) NOT NULL,
  `Registro_user_Cedula` INT NOT NULL,
  `Factura_Cod_Factura` INT NULL,
  `Factura_Adminstrador_ Cedula` INT NULL,
  PRIMARY KEY (`Registro_user_Cedula`),
  CONSTRAINT `fk_User_Particular_Registro_user`
    FOREIGN KEY (`Registro_user_Cedula`)
    REFERENCES `parqueaderoFacil`.`Registro_user` (`Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_Particular_Factura1`
    FOREIGN KEY (`Factura_Cod_Factura` , `Factura_Adminstrador_ Cedula`)
    REFERENCES `parqueaderoFacil`.`Factura` (`Cod_Factura` , `Adminstrador_ Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_User_Particular_Factura1_idx` ON `parqueaderoFacil`.`User_Particular` (`Factura_Cod_Factura` ASC, `Factura_Adminstrador_ Cedula` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Tipo_mensualidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Tipo_mensualidad` (
  `Dia` DATETIME NULL,
  `hora` DATETIME NULL,
  `mes` DATETIME NULL,
  `quincena` DATETIME NULL,
  `Registro_user_Cedula` INT NOT NULL,
  PRIMARY KEY (`Registro_user_Cedula`),
  CONSTRAINT `fk_Tipo_mensualidad_Registro_user1`
    FOREIGN KEY (`Registro_user_Cedula`)
    REFERENCES `parqueaderoFacil`.`Registro_user` (`Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Registro_entrada/salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Registro_entrada/salida` (
  `Hora_Entrada` DATETIME NOT NULL,
  `Hora_salida` DATETIME NOT NULL,
  `cantidad_time` VARCHAR(45) NOT NULL,
  `Factura_Cod_Factura` INT NOT NULL,
  `Factura_Adminstrador_ Cedula` INT NULL,
  PRIMARY KEY (`Factura_Cod_Factura`),
  CONSTRAINT `fk_Registro_entrada/salida_Factura1`
    FOREIGN KEY (`Factura_Cod_Factura` , `Factura_Adminstrador_ Cedula`)
    REFERENCES `parqueaderoFacil`.`Factura` (`Cod_Factura` , `Adminstrador_ Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parqueaderoFacil`.`Lugar_Disponible`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parqueaderoFacil`.`Lugar_Disponible` (
  `numero_puesto` VARCHAR(6) NOT NULL,
  `disponible` TINYINT(1) NOT NULL,
  `Factura_Cod_Factura` INT NOT NULL,
  `Factura_Adminstrador_ Cedula` INT NULL,
  PRIMARY KEY (`Factura_Cod_Factura`),
  CONSTRAINT `fk_Lugar_Disponible_Factura1`
    FOREIGN KEY (`Factura_Cod_Factura` , `Factura_Adminstrador_ Cedula`)
    REFERENCES `parqueaderoFacil`.`Factura` (`Cod_Factura` , `Adminstrador_ Cedula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `numero_puesto_UNIQUE` ON `parqueaderoFacil`.`Lugar_Disponible` (`numero_puesto` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
